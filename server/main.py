from fastai.vision.all import *
from flask import Flask, request, jsonify
from flask_cors import CORS, cross_origin
app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

cwd = os.getcwd()
path = Path(cwd + "/snake.pkl")
learn_inf = load_learner(path, cpu=True)

@app.route("/", methods=['GET', 'POST'])
@cross_origin()
def upload():
    uploaded_file = request.files['file']
    pred, pred_idx, probs = learn_inf.predict(uploaded_file.read())
    result = list(map(lambda x: (x[1], str(x[0].item())), sorted(list(zip(probs, learn_inf.dls.vocab)),
                  key=lambda x: x[0])[::-1]))
    return jsonify(result)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080)
