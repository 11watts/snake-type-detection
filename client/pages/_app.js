import { ThemeProvider } from "emotion-theming";
import theme from "@rebass/preset";
import "react-image-crop/dist/ReactCrop.css";

function MyApp({ Component, pageProps }) {
  return (
    <ThemeProvider theme={theme}>
      <Component {...pageProps} />
    </ThemeProvider>
  );
}
export default MyApp;
