import React from "react";
import { Box, Card, Image, Heading, Text, Button, Flex } from "rebass";
import { useDropzone } from "react-dropzone";
import Head from "next/head";
import ReactCrop from "react-image-crop";
import axios from "axios";

const pixelRatio = 4;
const URL = process.env.NEXT_PUBLIC_API_URL || "http://localhost:8080";

const getResizedCanvas = (canvas, newWidth, newHeight) => {
  const tmpCanvas = document.createElement("canvas");
  tmpCanvas.width = newWidth;
  tmpCanvas.height = newHeight;

  const ctx = tmpCanvas.getContext("2d");
  ctx.drawImage(
    canvas,
    0,
    0,
    canvas.width,
    canvas.height,
    0,
    0,
    newWidth,
    newHeight
  );

  return tmpCanvas;
};

const generateUpload = (previewCanvas, crop) => {
  return new Promise((resolve, reject) => {
    if (!crop || !previewCanvas) {
      return reject("missing");
    }
    const canvas = getResizedCanvas(previewCanvas, crop.width, crop.height);
    canvas.toBlob(
      blob => {
        return resolve(blob);
      },
      "image/png",
      1
    );
  });
};

const Home = () => {
  const [upImg, setUpImg] = React.useState();
  const imgRef = React.useRef(null);
  const previewCanvasRef = React.useRef(null);
  const [crop, setCrop] = React.useState({
    unit: "%",
    x: 10,
    y: 10,
    width: 80,
    height: 80
  });
  const [completedCrop, setCompletedCrop] = React.useState(null);
  const [loading, setLoading] = React.useState(false);
  const [results, setResults] = React.useState([]);
  const onDrop = React.useCallback(acceptedFiles => {
    if (acceptedFiles && acceptedFiles.length > 0) {
      const reader = new FileReader();
      reader.addEventListener("load", () => setUpImg(reader.result));
      reader.readAsDataURL(acceptedFiles[0]);
    }
  }, []);
  const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop });

  const onSubmit = React.useCallback(
    async e => {
      try {
        e.preventDefault();
        setLoading(true);
        const data = new FormData();
        const img = await generateUpload(
          previewCanvasRef.current,
          completedCrop
        );
        data.append("file", img);
        data.append("filename", "snake");
        const res = await axios.post(URL, data, {
          headers: { "content-type": "multipart/form-data" }
        });
        setResults(res.data);
        setLoading(false);
      } catch (error) {
        console.error(error);
      }
    },
    [completedCrop, previewCanvasRef]
  );

  const onLoad = React.useCallback(img => {
    imgRef.current = img;
  }, []);

  React.useEffect(() => {
    if (!completedCrop || !previewCanvasRef.current || !imgRef.current) {
      return;
    }

    const image = imgRef.current;
    const canvas = previewCanvasRef.current;
    const crop = completedCrop;

    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    const ctx = canvas.getContext("2d");

    canvas.width = crop.width * pixelRatio;
    canvas.height = crop.height * pixelRatio;

    ctx.setTransform(pixelRatio, 0, 0, pixelRatio, 0, 0);
    ctx.imageSmoothingEnabled = false;

    ctx.drawImage(
      image,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      crop.width,
      crop.height
    );
  }, [completedCrop]);

  return (
    <div>
      <Head>
        <title>Snake Classifier</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Flex alignItems="center" justifyContent="center">
        <Box width={800}>
          <Card>
            <Box px={2}>
              <Heading as="h3">Southeast Texas Snake Classifier</Heading>
              <Text fontSize={0}>
                Predicts what type of snake is in a photo{" "}
              </Text>
            </Box>
          </Card>
        </Box>
      </Flex>
      <Flex justifyContent="center">
        <Box width={800}>
          <Card
            sx={{
              p: 1,
              borderRadius: 2,
              boxShadow: "0 0 16px rgba(0, 0, 0, .25)"
            }}
          >
            <Box px={2}>
              <form method="POST" onSubmit={onSubmit}>
                <Box px={2} py={2}>
                  <div {...getRootProps()} className="dropzone">
                    <input {...getInputProps()} />
                    {isDragActive ? (
                      <Text>Drop the image...</Text>
                    ) : (
                      <Text>Drop your image or click here to select</Text>
                    )}
                  </div>
                </Box>
                <Flex justifyContent="center">
                  <Box>
                    <ReactCrop
                      src={upImg}
                      onImageLoaded={onLoad}
                      crop={crop}
                      onChange={c => setCrop(c)}
                      onComplete={c => setCompletedCrop(c)}
                    />
                  </Box>
                </Flex>
                <Flex justifyContent="center">
                  {upImg && !loading && (
                    <Button variant="primary" mr={2} type="submit">
                      Upload
                    </Button>
                  )}
                  {loading && (
                    <Box>
                      <div className="loader"></div>
                    </Box>
                  )}
                </Flex>
                {results.length > 0 && !loading && (
                  <Flex justifyContent="center">
                    <table>
                      <thead>
                        <tr>
                          <th>Type</th>
                          <th>Probability</th>
                        </tr>
                      </thead>
                      <tbody>
                        {results.slice(0, 5).map(i => (
                          <tr key={i[0]}>
                            <td>{i[0]}</td>
                            <td>{parseFloat(i[1]).toFixed(5)}</td>
                          </tr>
                        ))}
                      </tbody>
                    </table>
                  </Flex>
                )}
              </form>
            </Box>
          </Card>
        </Box>
      </Flex>
      <canvas ref={previewCanvasRef} />
      <style jsx>{`
        .dropzone {
          border-style: dotted;
          height: 48px;
          padding: 8px;
        }
        .dropzone:hover {
          border-color: grey;
          background-color: #eee;
        }
        canvas {
          display: none;
        }
        th,
        td {
          padding: 4px;
        }
        .loader {
          border: 4px solid #f3f3f3;
          border-radius: 50%;
          border-top: 4px solid #3498db;
          width: 24px;
          height: 24px;
          -webkit-animation: spin 2s linear infinite; /* Safari */
          animation: spin 2s linear infinite;
          margin-right: auto;
        }
        /* Safari */
        @-webkit-keyframes spin {
          0% {
            -webkit-transform: rotate(0deg);
          }
          100% {
            -webkit-transform: rotate(360deg);
          }
        }

        @keyframes spin {
          0% {
            transform: rotate(0deg);
          }
          100% {
            transform: rotate(360deg);
          }
        }
      `}</style>
    </div>
  );
};

export default Home;
