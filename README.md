# Snake Type Detection

An image classifier that guesses the species of snake that is in a photo. Trained on images of 39 snake species common in Southeast Texas. Created with Python, JavaScript, FastAI, Flask, and React. 


Currently deployed at: [http://149.28.125.226:3000/](http://149.28.125.226:3000/).